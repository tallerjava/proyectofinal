package com.tallerjava.sapo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SapoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SapoApplication.class, args);
    }
}
