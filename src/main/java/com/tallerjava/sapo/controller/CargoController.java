package com.tallerjava.sapo.controller;

import com.tallerjava.sapo.entidad.Cargo;
import com.tallerjava.sapo.service.CargoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CargoController {

    @Autowired
    private CargoService cargoService;

    @GetMapping(value = "/cargos")
    public String mostrarCargos(Model modelo) {
        List<Cargo> cargos = cargoService.obtenerTodos();
        modelo.addAttribute("cargos", cargos);
        return "cargos";
    }
}
