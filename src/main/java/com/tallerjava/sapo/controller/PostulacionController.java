package com.tallerjava.sapo.controller;

import com.tallerjava.sapo.entidad.Estado;
import com.tallerjava.sapo.entidad.Postulacion;
import com.tallerjava.sapo.service.PostulacionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class PostulacionController {

    @Autowired
    private PostulacionService postulacionService;

    @GetMapping(value = "/postulaciones")
    public String mostrarPostulaciones(Model modelo) {
        List<Postulacion> postulaciones = postulacionService.obtenerTodas();
        modelo.addAttribute("postulaciones", postulaciones);
        modelo.addAttribute("estados", Estado.values());
        return "postulaciones";
    }

    @PostMapping(value = "/postulaciones")
    public String guardarPostulacion(Postulacion postulacion, RedirectAttributes modelo) {
        try {
            postulacionService.guardar(postulacion);
            modelo.addFlashAttribute("mensajeOk", "Su postulacion ha sido procesada con exito!");
        } catch (IllegalArgumentException illegalArgument) {
            modelo.addFlashAttribute("mensajeError", illegalArgument.getMessage());
        }
        return "redirect:/cargos";
    }

    @GetMapping(value = "/postulaciones/misPostulaciones")
    public String mostrarMisPostulaciones(String email, RedirectAttributes modelo, Model model) {
        try {
            List<Postulacion> misPostulaciones = postulacionService.obtenerMisPostulaciones(email);
            model.addAttribute("misPostulaciones", misPostulaciones);
            return "misPostulaciones";
        } catch (IllegalArgumentException illegalArgument) {
            modelo.addFlashAttribute("mensajeError", illegalArgument.getMessage());
            return "redirect:/cargos";
        }
    }
}
