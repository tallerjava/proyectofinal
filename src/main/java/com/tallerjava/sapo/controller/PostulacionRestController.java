package com.tallerjava.sapo.controller;

import com.tallerjava.sapo.entidad.Estado;
import com.tallerjava.sapo.service.PostulacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PostulacionRestController {

    @Autowired
    private PostulacionService postulacionService;

    @RequestMapping(value = "/postulacion/{id}", method = RequestMethod.PUT)
    public void actualizarPostulacion(@PathVariable Long id, @RequestBody Estado estado) {
        postulacionService.actualizar(id, estado);
    }
}
