package com.tallerjava.sapo.entidad;

public enum Estado {
    EN_PROCESO,
    RECHAZADO,
    CONFIRMADO
}
