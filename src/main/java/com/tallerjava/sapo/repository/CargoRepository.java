package com.tallerjava.sapo.repository;

import com.tallerjava.sapo.entidad.Cargo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CargoRepository extends JpaRepository<Cargo, Long> {
    
}
