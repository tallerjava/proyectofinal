package com.tallerjava.sapo.repository;

import com.tallerjava.sapo.entidad.Cargo;
import com.tallerjava.sapo.entidad.Postulacion;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PostulacionRepository extends JpaRepository<Postulacion, Long> {

    public boolean existsByEmailAndCargo(String email, Cargo cargo);

    public List<Postulacion> findByEmail(String email);

}
