package com.tallerjava.sapo.service;

import com.tallerjava.sapo.entidad.Cargo;
import java.util.List;

public interface CargoService {

    public List<Cargo> obtenerTodos();
}
