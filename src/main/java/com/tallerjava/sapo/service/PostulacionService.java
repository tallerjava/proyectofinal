package com.tallerjava.sapo.service;

import com.tallerjava.sapo.entidad.Estado;
import com.tallerjava.sapo.entidad.Postulacion;
import java.util.List;

public interface PostulacionService {

    public Postulacion guardar(Postulacion postulacion);

    public List<Postulacion> obtenerTodas();

    public List<Postulacion> obtenerMisPostulaciones(String email);

    public Postulacion actualizar(Long id, Estado estado);

}
