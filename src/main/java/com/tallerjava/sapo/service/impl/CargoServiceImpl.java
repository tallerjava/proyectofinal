package com.tallerjava.sapo.service.impl;

import com.tallerjava.sapo.repository.CargoRepository;
import com.tallerjava.sapo.entidad.Cargo;
import com.tallerjava.sapo.service.CargoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CargoServiceImpl implements CargoService {

    @Autowired
    private CargoRepository cargoRepository;

    @Override
    public List<Cargo> obtenerTodos() {
        return cargoRepository.findAll();
    }

}
