package com.tallerjava.sapo.service.impl;

import com.tallerjava.sapo.entidad.Cargo;
import com.tallerjava.sapo.entidad.Estado;
import com.tallerjava.sapo.entidad.Postulacion;
import com.tallerjava.sapo.repository.CargoRepository;
import com.tallerjava.sapo.repository.PostulacionRepository;
import com.tallerjava.sapo.service.PostulacionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PostulacionServiceImpl implements PostulacionService {

    @Autowired
    private PostulacionRepository postulacionRepository;

    @Autowired
    private CargoRepository cargoRepository;

    @Override
    public Postulacion guardar(Postulacion postulacion) {
        String email = postulacion.getEmail();
        Cargo cargo = postulacion.getCargo();

        if (email == null) {
            throw new IllegalArgumentException("Necesita ingresar un mail para postularse");
        }
        if (cargo == null) {
            throw new IllegalArgumentException("Necesita elegir un cargo para postularse");
        }

        if (!cargoRepository.existsById(cargo.getId())) {
            throw new IllegalArgumentException("El cargo seleccionado no existe");
        }

        if (postulacionRepository.existsByEmailAndCargo(email, cargo)) {
            throw new IllegalArgumentException("Ya se ha postulado al cargo");
        }
        if (!email.matches(".*@.*")) {
            throw new IllegalArgumentException("El email ingresado no corresponde a uno valido");
        }
        return postulacionRepository.save(postulacion);
    }

    @Override
    public List<Postulacion> obtenerTodas() {
        return postulacionRepository.findAll();
    }

    @Override
    public List<Postulacion> obtenerMisPostulaciones(String email) {
        if (email == null) {
            throw new IllegalArgumentException("El email ingresado no se encuentra registrado");
        }
        if (!email.matches(".*@.*")) {
            throw new IllegalArgumentException("El email ingresado no es valido");
        } else {
            return postulacionRepository.findByEmail(email);
        }
    }

    @Override
    public Postulacion actualizar(Long id, Estado estado) {
        Postulacion postulacion = postulacionRepository.getOne(id);
        postulacion.setEstado(estado);
        return postulacionRepository.save(postulacion);
    }
 
}
