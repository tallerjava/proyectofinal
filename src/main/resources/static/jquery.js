$(document).ready(function () {
    $(".js-estado-item").click(function (event) {

        var id = $(this).data("id-postulacion");
        var estado = $(this).data("estado-postulacion");

        event.preventDefault();
        $.ajax("/postulacion/" + id, {
            type: "PUT",
            data: JSON.stringify(estado),
            contentType: "application/json",
            success: function () {
                $(".js-estado-postulacion").text(estado);
            }
        });
    });
});

