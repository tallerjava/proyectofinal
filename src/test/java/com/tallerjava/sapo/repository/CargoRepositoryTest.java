package com.tallerjava.sapo.repository;

import com.tallerjava.sapo.SapoApplicationTests;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.beans.factory.annotation.Autowired;

public class CargoRepositoryTest extends SapoApplicationTests {

    @Autowired
    private CargoRepository cargoRepository;

    @Test
    public void findAll_variosCargos_retornaListaDeCargos() {
        assertFalse(cargoRepository.findAll().isEmpty());
    }

}
