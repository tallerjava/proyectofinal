package com.tallerjava.sapo.service;

import com.tallerjava.sapo.SapoApplicationTests;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.beans.factory.annotation.Autowired;

public class CargoServiceImplTest extends SapoApplicationTests {

    @Autowired
    private CargoService cargoService;

    @Test
    public void obtenerTodos_variosCargos_retornaListaDeCargos() {
        assertFalse(cargoService.obtenerTodos().isEmpty());

    }
}
