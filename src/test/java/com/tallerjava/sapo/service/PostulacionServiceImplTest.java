package com.tallerjava.sapo.service;

import com.tallerjava.sapo.SapoApplicationTests;
import com.tallerjava.sapo.entidad.Cargo;
import com.tallerjava.sapo.entidad.Estado;
import com.tallerjava.sapo.entidad.Postulacion;
import com.tallerjava.sapo.repository.CargoRepository;
import com.tallerjava.sapo.repository.PostulacionRepository;
import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class PostulacionServiceImplTest extends SapoApplicationTests {

    @Autowired
    PostulacionService postulacionService;

    @Autowired
    CargoRepository cargoRepository;

    @Autowired
    PostulacionRepository postulacionRepository;

    @Test
    public void guardar_docenteNoRegistradoSePostulaAlCargo_seGuardaLaPostulacion() {
        Cargo cargo = cargoRepository.getOne(1L);
        Postulacion postulacionEsperada = new Postulacion();
        postulacionEsperada.setEmail("acuaman@gmail.com");
        postulacionEsperada.setCargo(cargo);
        postulacionEsperada.setEstado(Estado.EN_PROCESO);
        Postulacion postulacionObtenida = postulacionService.guardar(postulacionEsperada);
        assertNotNull(postulacionObtenida.getId());
    }

    @Test
    public void guardar_docenteYaRegistradoSePostulaOtroCargo_seGuardaLaPostulacion() {
        Cargo cargo = cargoRepository.getOne(2L);
        Postulacion postulacionEsperada = new Postulacion();
        postulacionEsperada.setEmail("superman@gmail.com");
        postulacionEsperada.setCargo(cargo);
        postulacionEsperada.setEstado(Estado.EN_PROCESO);
        Postulacion postulacionObtenida = postulacionService.guardar(postulacionEsperada);
        assertNotNull(postulacionObtenida.getId());
    }

    @Test
    public void guardar_docenteSePostulaAlMismoCargo_retornaUnaException() {
        try {
            Postulacion postulacion = new Postulacion();
            Cargo cargo = cargoRepository.getOne(1L);
            postulacion.setCargo(cargo);
            postulacion.setEmail("superman@gmail.com");
            postulacionService.guardar(postulacion);
        } catch (IllegalArgumentException illegalArgument) {
            assertEquals("Ya se ha postulado al cargo", illegalArgument.getMessage());
        } catch (Exception ex) {
            fail("Se recibio una excepcion no esperada");
        }

    }

    @Test
    public void guardar_sinEmailDelDocente_retornaUnaException() {
        try {
            Postulacion postulacion = new Postulacion();
            Cargo cargo = cargoRepository.getOne(1L);
            postulacion.setCargo(cargo);
            postulacionService.guardar(postulacion);
            fail("Se guardo la postulacion");
        } catch (IllegalArgumentException illegalArgument) {
            assertEquals("Necesita ingresar un mail para postularse", illegalArgument.getMessage());
        } catch (Exception ex) {
            fail("Se recibio una excepcion no esperada");
        }
    }

    @Test
    public void guardar_emailDelDocenteInvalido_retornaUnaException() {
        try {
            Postulacion postulacion = new Postulacion();
            Cargo cargo = cargoRepository.getOne(1L);
            postulacion.setCargo(cargo);
            postulacion.setEmail("");
            postulacionService.guardar(postulacion);
            fail("Se guardo la postulacion");
        } catch (IllegalArgumentException illegalArgument) {
            assertEquals("El email ingresado no corresponde a uno valido", illegalArgument.getMessage());
        } catch (Exception ex) {
            fail("Se recibio una excepcion no esperada");
        }
    }

    @Test
    public void guardar_sinElegirUnCargo_retornaUnaException() {
        try {
            Postulacion postulacion = new Postulacion();
            postulacion.setEmail("superman@gmail.com");
            postulacionService.guardar(postulacion);
            fail("Se guardo la postulacion");
        } catch (IllegalArgumentException illegalArgument) {
            assertEquals("Necesita elegir un cargo para postularse", illegalArgument.getMessage());
        } catch (Exception ex) {
            fail("Se recibio una excepcion no esperada");
        }
    }

    @Test
    public void guardar_elCargoNoExiste_retornaUnaException() {
        try {
            Cargo cargo = new Cargo();
            cargo.setAsignatura("Maestro de plastica");
            cargo.setDistrito("Avellaneda");
            cargo.setEscuela("Padre Marquez");
            cargo.setHorario("Jue 15:00hs a 11:00hs");
            cargo.setNivel("Nivel Incial");
            cargo.setId(55L);
            Postulacion postulacion = new Postulacion();
            postulacion.setEmail("superman@gmail.com");
            postulacion.setCargo(cargo);
            postulacionService.guardar(postulacion);
            fail("Se guardo la postulacion");
        } catch (IllegalArgumentException illegalArgument) {
            assertEquals("El cargo seleccionado no existe", illegalArgument.getMessage());
        }
    }

    @Test
    public void obtenerTodas_variasPostulaciones_retornarListadoDePostulaciones() {
        assertFalse(postulacionService.obtenerTodas().isEmpty());
    }

    @Test
    public void obtenerMisPostulaciones_emailRegistrado_retornaListadoDePostulaciones() {
        assertFalse(postulacionService.obtenerMisPostulaciones("superman@gmail.com").isEmpty());
    }

    @Test
    public void obtenerTodasMisPostulaciones_emailNull_retornaException() {
        try {
            postulacionService.obtenerMisPostulaciones(null);
            fail("El email ingresado se encuentra registrado");
        } catch (IllegalArgumentException illegalArgument) {
            assertEquals("El email ingresado no se encuentra registrado", illegalArgument.getMessage());
        }
    }

    @Test
    public void obtenerTodasMisPostulaciones_emailIncorrecto_retornaException() {
        try {
            postulacionService.obtenerMisPostulaciones("sarasa#sarasa.com");
            fail("El email ingresado se encuentra registrado");
        } catch (IllegalArgumentException illegalArgument) {
            assertEquals("El email ingresado no es valido", illegalArgument.getMessage());
        }
    }

    @Test
    public void actualizar_estadoEnProcesoAConfirmado_seActualizaElEstado() {
        Postulacion postulacion = postulacionService.actualizar(1L, Estado.CONFIRMADO);
        assertEquals(Estado.CONFIRMADO, postulacion.getEstado());
    }

    @Test
    public void actualizar_estadoEnProcesoARechazado_seActualizaElEstado() {
        Postulacion postulacion = postulacionService.actualizar(1L, Estado.RECHAZADO);
        assertEquals(Estado.RECHAZADO, postulacion.getEstado());
    }

    @Test
    public void actualizar_estadoRechazadoAEnProceso_seActualizaElEstado() {
        Postulacion postulacion = postulacionService.actualizar(2L, Estado.EN_PROCESO);
        assertEquals(Estado.EN_PROCESO, postulacion.getEstado());
    }

    @Test
    public void actualizar_estadoRechazadoAConfirmado_seActualizaElEstado() {
        Postulacion postulacion = postulacionService.actualizar(2L, Estado.CONFIRMADO);
        assertEquals(Estado.CONFIRMADO, postulacion.getEstado());
    }

    @Test
    public void actualizar_estadoConfirmadoAEnProceso_seActualizaElEstado() {
        Postulacion postulacion = postulacionService.actualizar(3L, Estado.EN_PROCESO);
        assertEquals(Estado.EN_PROCESO, postulacion.getEstado());
    }

    @Test
    public void actualizar_estadoConfirmadoARechazado_seActualizaElEstado() {
        Postulacion postulacion = postulacionService.actualizar(3L, Estado.RECHAZADO);
        assertEquals(Estado.RECHAZADO, postulacion.getEstado());
    }
}
