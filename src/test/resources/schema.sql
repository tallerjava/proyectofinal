CREATE TABLE cargo (
    id Integer AUTO_INCREMENT,
    nivel VARCHAR(50) NOT NULL,
    asignatura VARCHAR(100) NOT NULL,
    escuela VARCHAR(50) NOT NULL,
    distrito VARCHAR(50) NOT NULL,
    horario VARCHAR(50) NOT NULL,
    puntaje Smallint,
    PRIMARY KEY (id)
);

CREATE TABLE postulacion (
    id Integer AUTO_INCREMENT,
    cargo_id Integer NOT NULL,
    email VARCHAR(50) NOT NULL,
    estado Integer NOT NULL DEFAULT 0,
    FOREIGN KEY (cargo_id) REFERENCES cargo(id) ON UPDATE CASCADE,
    PRIMARY KEY (id),
    UNIQUE(email, cargo_id)
);
